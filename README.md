# Simple Deployment
## Build
`. install.sh`

## Run
As a python service:

`python3 main.py`

Through Gunicorn's http server:

`gunicorn -c config/gunicorn_config.py app:webapp`

# Docker Deployment
## Build
`sh docker_build.sh`

## Run
`sh docker_run.sh`


# Tests
`sh run_tests.sh`

# Coverage
`sh check_coverage.sh`