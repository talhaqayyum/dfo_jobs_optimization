import json

from flask import Blueprint, request, Response, jsonify
from flask_cors import cross_origin

from ..auth import auth
from ..modules.jobs_optimization.job_optimization import jobs_assignment
from ..modules.jobs_verification.jobs_verification import jobs_verification

jobs_blueprint = Blueprint('jobs',
                           __name__,
                           url_prefix='/jobs',
                           )


@jobs_blueprint.route('/', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
@auth.login_required
def index():
    return jsonify({"hello": "world"})


@jobs_blueprint.route('/assign', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
@auth.login_required
def report():
    # request_obj = request.get_json(force=True)

    response_obj = jobs_assignment()

    if not response_obj['status'] == 200:
        response_payload = json.dumps({"message": response_obj["message"]})
        return Response(response=response_payload,
                        status=response_obj['status'],
                        content_type='application/json')

    response_payload = json.dumps(response_obj["message"])
    return Response(response=response_payload,
                    status=200,
                    content_type='application/json')


@jobs_blueprint.route('/verify', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
@auth.login_required
def report_():
    request_obj = request.get_json(force=True)

    response_obj = jobs_verification(request_obj)

    if not response_obj['status'] == 200:
        response_payload = json.dumps({"message": response_obj["message"]})
        return Response(response=response_payload,
                        status=response_obj['status'],
                        content_type='application/json')

    response_payload = json.dumps(response_obj["message"])
    return Response(response=response_payload,
                    status=200,
                    content_type='application/json')
