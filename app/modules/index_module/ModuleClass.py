def default(x=True):
    if x:
        return "Default page for verifications backend"
    else:
        return "Default class"
