import psycopg2
from config import config
import pandas as pd


def get_connection():
    print("Connecting to Database...")
    conn = psycopg2.connect(
        host=config.db_host_prod,
        port=config.db_port_prod,
        user=config.db_username_prod,
        password=config.db_password_prod,
        dbname=config.db_database_prod
    )
    return conn


def close_connection(cur, conn):
    cur.close()
    conn.close()


def get_db_cursor():
    conn = get_connection()
    cur = conn.cursor()
    return cur, conn


def get_jobs_data(cur, assignee_id, job_id):
    query_job_logs = f'''

    select j.id, j."applicantId" , j."assignee", alr."loanId" , d."name" ,j."jobCompletedAt" as "Job Verification Date" , j."jobType" ,
    j.lat as "cust_lat", j.lng as "cust_lng",il.lat as "dfo_lat", il.lng as "dfo_lng", il2.lat as "customer_lat", il2.lng as "customer_lng",
    j."job"->>'image_gate' as "dfo_gateimage" , ard."electricMeterPic" as "customer_metre_pic", j."job"->>'verification_outcome' as "verification outcome"
    from jobs j
    left join public.image_locations il on j."job"->>'image_gate' = il.url  
    join "applicantsLoanRequests" alr on alr."applicantId" = j."applicantId" 
    join "ApiApplicants" aa on aa."applicantId" = alr."applicantId" 
    join "ApplicantResidentialDetails" ard on ard."applicantId" = aa.id 
    join image_locations il2 on il2.url = ard."electricMeterPic" 
    join "DFOs" d on d.id = j.assignee 
    join "ApiUsers" au on au."applicantId" = alr."applicantId" 
    where j.assignee = '{assignee_id}' and j.id = '{job_id}' and
    j."jobCompletedAt" between (current_date - INTERVAL '2 day') and Now() 
    and j.assignee notnull 
    order by j."jobCompletedAt" desc


    '''
    #     print(query_job_logs)
    cur.execute(query_job_logs)
    df = pd.DataFrame(list(cur.fetchall()),
                      columns=['jobId', 'applicantId', 'assigneeId', 'loanId', 'dfo_name', 'Job Verification Date',
                               'jobType',
                               'job_lat', 'job_lng', 'dfo_lat', 'dfo_lng', 'customer_lat', 'customer_lng',
                               'dfo_gateimage', 'customer_metre_pic', 'verification outcome'])
    print("Table Fetched...................................")
    return df
