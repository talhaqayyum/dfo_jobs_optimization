from app.modules.jobs_verification.data_fetching_funcs import get_job_verification
from app.modules.jobs_verification.db_helper import get_jobs_data, get_db_cursor


required_keys = ["assignee_id", "job_id"]


def get_response_object(status, message):
    response_obj = {
        "status": status,
        "message": message
    }
    return response_obj


def jobs_verification(request):
    # Pre-process request object
    matched_keys = list(set(request.keys()).intersection(required_keys))
    if not len(matched_keys) == len(required_keys):
        return {
            "message": "Required fields missing",
            "status": 400
        }
    cur, conn = get_db_cursor()
    df = get_jobs_data(cur, assignee_id=request["assignee_id"], job_id=request["job_id"])
    distance_from_job = get_job_verification(df, 1)
    distance_from_cust_and_metre = get_job_verification(df, 0)

    verification_response = {
        "dfo_distance_from_job (km)": distance_from_job,
        "cust_distance_from_metre (km)": distance_from_cust_and_metre
    }
    return get_response_object(status=200, message=verification_response)
