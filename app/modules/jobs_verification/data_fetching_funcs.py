import geopy.distance as gd


def get_lat_long(df):
    job_lattitude = list(df.customer_lat)
    job_longitude = list(df.customer_lng)
    dfo_lattitude = list(df.dfo_lat)
    dfo_longitude = list(df.dfo_lng)
    return job_lattitude, job_longitude, dfo_lattitude, dfo_longitude


def get_lat_long_cust(df):
    cust_lattitude = list(df.customer_lat)
    cust_longitude = list(df.customer_lng)
    job_lattitude = list(df.job_lat)
    job_longitude = list(df.job_lng)
    return job_lattitude, job_longitude, cust_lattitude, cust_longitude


def get_job_verification(df, flag):
    if flag:
        job_lattitude, job_longitude, dfo_lattitude, dfo_longitude = get_lat_long(df)
    else:
        job_lattitude, job_longitude, dfo_lattitude, dfo_longitude = get_lat_long_cust(df)
    if None not in (job_lattitude[0], job_longitude[0], dfo_lattitude[0], dfo_longitude[0]):
        coords_1 = (job_lattitude[0], job_longitude[0])
        coords_2 = (dfo_lattitude[0], dfo_longitude[0])
        distance_from_job = gd.GeodesicDistance(coords_1, coords_2).km
        return distance_from_job

    else:
        print("latlong not available")
        return None
