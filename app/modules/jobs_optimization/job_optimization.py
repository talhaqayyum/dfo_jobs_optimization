import itertools
from app.modules.jobs_optimization.data_fetching_funcs import fetch_jobs_data, fetch_dfo_list, filter_jobs, \
    filter_dfo_loc, \
    get_assignee_job_details
from config import config


def get_response_object(status, message):
    response_obj = {
        "status": status,
        "message": message
    }
    return response_obj


def distribute_work(dfos, tasks):
    dfo_count = len(dfos)
    dfo_ids = range(dfo_count)
    all_tasks_for_all_dfos = itertools.tee(tasks, dfo_count)
    assignments = [(dfos[id], itertools.islice(i, id, None, dfo_count))
                   for (id, i) in enumerate(all_tasks_for_all_dfos)]
    return assignments


def get_assignments(verification_officers, job_list):
    assigned_jobs = []
    number_of_jobs_to_assign = range(len(verification_officers))
    for (worker, tasks) in distribute_work(verification_officers, job_list):
        job = list(tasks)
        jobs = {
            "Dfo_id": worker,
            "jobs_assigned": job
        }
        assigned_jobs.append(jobs)
    # get_assignee_job_details(assigned_jobs)
    return assigned_jobs


def jobs_assignment():
    city_keys = ['Karachi', 'Lahore', 'Islamabad', 'Rawalpindi']
    jobs = fetch_jobs_data()
    dfo_list = fetch_dfo_list()
    jobs.loc[jobs['city'].str.contains('Rawalpindi'), ['city']] = 'Islamabad'  # Replace Rawalpindi with Islamabad
    assigned_job = []
    for keys in city_keys:
        assigned_jobs = get_assignments(verification_officers=filter_dfo_loc(dfo_list, keys),
                                        job_list=filter_jobs(keys, jobs))

        jobs_response = {
            keys: assigned_jobs
        }
        assigned_job.append(jobs_response)
        # print("jobs assigned")
    return get_response_object(status=200, message=assigned_job)
