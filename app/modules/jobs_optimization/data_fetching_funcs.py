import pandas as pd
from app.modules.jobs_optimization import db_helper as db_helper


def fetch_jobs_data():
    cur, conn = db_helper.get_db_cursor()
    df_jobs = db_helper.get_jobs_data(cur=cur)
    return df_jobs


def fetch_dfo_list():
    cur, conn = db_helper.get_db_cursor()
    dfo_list = db_helper.get_dfo_list(cur=cur)
    return dfo_list


def filter_dfo_loc(dfo_list, dfo_location):
    vo_location = dfo_list.loc[dfo_list.city == dfo_location]
    vo_location = list(vo_location['dfo_id'].unique())
#     print(vo_location)
    return vo_location


def get_assignee_job_details(job_assignments):
    for i in range(len(job_assignments)):
        assignee = job_assignments[i]['Dfo_id']
        jobids = job_assignments[i]['jobs_assigned']
        db_helper.insert_assigned_jobs(assignee, jobids)
    print("All jobs Assigned")
    return None


def filter_jobs(city_name, jobs):
    city_jobs = jobs.loc[jobs['city'] == city_name]
    city_jobs = list(city_jobs['jobId'])
    #     print(city_jobs)
    return city_jobs

