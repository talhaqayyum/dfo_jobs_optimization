import psycopg2
from config import config
import pandas as pd


def get_connection():
    print("Connecting to Database...")
    conn = psycopg2.connect(host=config.db_host_prod, port=config.db_port_prod, user=config.db_username_prod,
                            password=config.db_password_prod, dbname=config.db_database_prod)
    return conn


def close_connection(cur, conn):
    cur.close()
    conn.close()


def get_db_cursor():
    conn = get_connection()
    cur = conn.cursor()
    return cur, conn


def get_jobs_data(cur):
    query_job_logs = f''' 
    SELECT * FROM ( SELECT distinct on (j."id") j."id", j."applicantId", j."jobType", 
    j.assignee , j."lat" , j."lng", j."address" , ard.city , aa."partnerId", j."createdAt", acr."tasdeeqSummary" ,
    acr."tasdeeqSummary"->'category_status'->'customer_category_status' as "category_status", alr."loanApproved" , 
    j.assignee FROM jobs j join "applicantsLoanRequests" alr on j."applicantId" = alr."applicantId" join 
    "ApiApplicants" aa on alr."applicantId" = aa."applicantId" join public."ApplicantResidentialDetails" ard on 
    ard."applicantId" = aa."id" join "apiEcibRecords" acr on acr."applicantId" = aa."applicantId" where 
    aa."partnerId" <> 6 and aa."partnerId" <> 5 or aa."partnerId" is null and j."createdAt" > '2022-01-01' and 
    ard."city" IN ('Lahore','Rawalpindi','Islamabad', 'Karachi') and j.assignee is null ) z_q WHERE 
    z_q."tasdeeqSummary" notnull AND (z_q.category_status IN ('"review"'::jsonb)) and z_q."loanApproved" = 0 
'''
    print(query_job_logs)
    cur.execute(query_job_logs)
    df = pd.DataFrame(list(cur.fetchall()), columns=['jobId', 'applicantId', 'jobType', 'assignee', 'lat', 'lng',
                                                     'address', 'city', 'partner_id', 'createdAt', 'tasdeeqSummary',
                                                     'category_status', 'loanApproved', 'assignee'])
    return df


def get_dfo_list(cur):
    query_dfo = f'''

    select d.id,  d.city , djt."JobTypeId" , jt."name", d."name" 
    from public."DFOs" d 
    join public."dfo_jobTypes" djt on djt."DFOId" = d.id 
    join public."JobTypes" jt on jt.id = djt."JobTypeId" 
    where djt."JobTypeId" <> '3' and d.id not in ('b00a8414-2510-81c0-102a-fe90604df560') and d.city notnull 

    '''

    print(query_dfo)
    cur.execute(query_dfo)
    df = pd.DataFrame(list(cur.fetchall()), columns=['dfo_id', 'city', 'jobid', 'jobtype', 'dfo_name'])
    return df


def insert_assigned_jobs(assignee_id, jobids):
    list_assignee_details = [tuple([assignee_id])]
    cur, conn = get_db_cursor()
    for ids in jobids:
        query_assignee_details_insert = f"UPDATE public.jobs SET assignee = '{assignee_id}' WHERE id = '{ids}'"
        try:
            cur.execute(query_assignee_details_insert, list_assignee_details)
            conn.commit()
        except Exception as e:
            print(e)
    close_connection(cur=cur, conn=conn)
    #     print(query_assignee_details_insert)
    return None
