from flask import Flask
from flask_cors import CORS

from .controllers.index_controller import module
from .controllers.jobs_controller import jobs_blueprint


webapp = Flask(__name__)
CORS(webapp)
webapp.config.from_object('config.config')

webapp.register_blueprint(module)
webapp.register_blueprint(jobs_blueprint)

