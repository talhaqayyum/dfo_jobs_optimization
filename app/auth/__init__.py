import json

from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash

auth = HTTPBasicAuth()

with open("./app/auth/users_list.json", "r") as jsonfile:
    users_list = json.load(jsonfile)

users = {}
for user in users_list:
    user_name = user["username"]
    users[user_name] = generate_password_hash(user["password"])


@auth.verify_password
def verify_password(username, password):
    if username in users and check_password_hash(users.get(username), password):
        return username
