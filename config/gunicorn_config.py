"""gunicorn WSGI server configuration"""

hostname = '0.0.0.0'
port = '4030'
bind = f"{hostname}:{port}"
max_requests = 100
workers = 1
reload = True
