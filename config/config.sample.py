import os

# Statement for enabling the development environment
DEBUG = False

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection against *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "this_is_a_dummy_secret_key"

APP_INSTANCE = "scorecarding-service"

APP_VERSION = 1

SQLALCHEMY_DATABASE_URI = 'sqlite://'
DATABASE_CONNECT_OPTIONS = {}
REMEMBER_COOKIE_HTTPONLY = True

# App credentials
app_host = '0.0.0.0'
app_port = 4030

# MYSQL Database credentials
db_host = 'dev.creditfix.pk'
db_port = 3306
db_username = 'root'
db_password = 'creditfix@123'
db_database = 'internal-v2-db'

# API specs for fetching internal applicant data
hostname_internal_backend = "https://dev-in-v2-vendor-api.creditfix.pk/api/v2/applicant"
endpoint_applicant_login = "/login"
endpoint_applicant_all_data = "/getApplicantAllData"


# API specs for bank statement from SMS
hostname_sms_parser = "http://18.139.108.127:4005"
endpoint_sms_parser = "/sms_parser"
username_sms_parser = "admin"
password_sms_parser = "admin@123"

# json files for combination of tables and scores for personal scorecard
age_score_list_path = "./app/modules/score_card/personal/age.json"
education_score_list_path = "./app/modules/score_card/personal/education.json"
residence_score_list_path = "./app/modules/score_card/personal/residence.json"
per_capita_consumption_list_path = "./app/modules/score_card/personal/per_capita_consumption.json"

# json files for combination of tables and scores for work experience scorecard
business_exp_roa_list_path = "./app/modules/score_card/work_exp/business_exp_roa.json"
business_size_roa_list_path = "./app/modules/score_card/work_exp/business_size_roa.json"
current_job_exp_list_path = "./app/modules/score_card/work_exp/current_job_exp.json"
job_type_list_path = "./app/modules/score_card/work_exp/job_type.json"
seniority_level_total_exp_list_path = "./app/modules/score_card/work_exp/seniority_level_total_exp.json"

# json files for combination of tables and scores for financial scorecard
debt_to_income_ratio_list_path = "./app/modules/score_card/financial/debt_income_ratio.json"
expense_to_income_ratio_list_path = "./app/modules/score_card/financial/expense_income_ratio.json"
income_dependents_list_path = "./app/modules/score_card/financial/income_dependents.json"
income_family_expense_ratio_list_path = "./app/modules/score_card/financial/income_family_expense_ratio.json"
product_markup_list_path = "./app/modules/score_card/financial/product_markup.json"

# json files for combination of tables and scores for payment_behaviour scorecard
arrears_months_list_path = "./app/modules/score_card/payment_behaviour/arrear_months.json"
response_ecib = "./app/modules/score_card/payment_behaviour/tasdeeq_response_sample.json"
dpd_score_list_path = "./app/modules/score_card/payment_behaviour/dpd_score.json"
